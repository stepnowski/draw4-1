import java.awt.Graphics;
import javax.swing.JPanel;

public class DrawPanel extends JPanel
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// paintComponent needed with every JPanel
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);//1st statement needed with paintComponent
		
		int width = getWidth(); //public method of JPanel
		int height = getHeight(); // public method of JPanel
		
		//g.drawLine(0, 0, width/15,14*( height/15));
		
		for(int i = 1;i<=15;i++)
		{
			g.drawLine(0, 0,(i*( width/15)),((15-i)*( height/15)));
			g.drawLine(width, height,(i*( width/15)),((15-i)*( height/15)));
			g.drawLine(0, height,(i*( width/15)),((i)*( height/15)));
			g.drawLine(width, 0,(i*( width/15)),((i)*( height/15)));
		}
		
		
	}
}
